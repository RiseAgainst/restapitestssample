package com.test.objects.response;

/**
 * Entry point for soft assertions of different data types.
 */
@javax.annotation.Generated(value="assertj-assertions-generator")
public class SoftAssertions extends org.assertj.core.api.SoftAssertions {

  /**
   * Creates a new "soft" instance of <code>{@link com.test.objects.response.PostCommentAssert}</code>.
   *
   * @param actual the actual value.
   * @return the created "soft" assertion object.
   */
  @org.assertj.core.util.CheckReturnValue
  public com.test.objects.response.PostCommentAssert assertThat(com.test.objects.response.PostComment actual) {
    return proxy(com.test.objects.response.PostCommentAssert.class, com.test.objects.response.PostComment.class, actual);
  }

  /**
   * Creates a new "soft" instance of <code>{@link com.test.objects.response.PostsAssert}</code>.
   *
   * @param actual the actual value.
   * @return the created "soft" assertion object.
   */
  @org.assertj.core.util.CheckReturnValue
  public com.test.objects.response.PostsAssert assertThat(com.test.objects.response.Posts actual) {
    return proxy(com.test.objects.response.PostsAssert.class, com.test.objects.response.Posts.class, actual);
  }

}
