package com.test.objects.response;

/**
 * Entry point for assertions of different data types. Each method in this class is a static factory for the
 * type-specific assertion objects.
 */
@javax.annotation.Generated(value="assertj-assertions-generator")
public class Assertions {

  /**
   * Creates a new instance of <code>{@link com.test.objects.response.PostCommentAssert}</code>.
   *
   * @param actual the actual value.
   * @return the created assertion object.
   */
  @org.assertj.core.util.CheckReturnValue
  public static com.test.objects.response.PostCommentAssert assertThat(com.test.objects.response.PostComment actual) {
    return new com.test.objects.response.PostCommentAssert(actual);
  }

  /**
   * Creates a new instance of <code>{@link com.test.objects.response.PostsAssert}</code>.
   *
   * @param actual the actual value.
   * @return the created assertion object.
   */
  @org.assertj.core.util.CheckReturnValue
  public static com.test.objects.response.PostsAssert assertThat(com.test.objects.response.Posts actual) {
    return new com.test.objects.response.PostsAssert(actual);
  }

  /**
   * Creates a new <code>{@link Assertions}</code>.
   */
  protected Assertions() {
    // empty
  }
}
