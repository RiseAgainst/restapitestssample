package api;

import com.test.objects.response.Posts;

import static io.restassured.RestAssured.when;

public class PostApi {

    public static Posts getPostById(int id) {
        return   when()
                .get("posts/" + id)
                .then().log().all()
                .extract()
                .response().as(Posts.class);
    }
}
