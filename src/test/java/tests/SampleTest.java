package tests;

import api.PostApi;
import com.test.objects.response.SoftAssertions;
import io.restassured.RestAssured;
import org.testng.annotations.BeforeClass;

import org.testng.annotations.Test;

import com.test.objects.response.Posts;



public class SampleTest {

    @BeforeClass
    public void setUp() throws Exception {
        RestAssured.baseURI = "http://jsonplaceholder.typicode.com/";
        //RestAssured.basePath;
    }

    @Test
    public void defaultTest() {
        Posts posts = PostApi.getPostById(1);
    /*   PostsAssert.assertThat(posts)
                .hasUserId(1)
                .hasTitle("qq")
                .hasId(2);*/
        SoftAssertions softAssertions = new SoftAssertions();
        softAssertions.assertThat(posts)
                .hasUserId(1)
                .hasTitle("qq")
                .hasId(2);
        softAssertions.assertAll();

    }

    // for post request
    // given().body().post()
}
